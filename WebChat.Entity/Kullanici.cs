﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace WebChat.Entity
{
    public class Kullanici
    {
        [Key]
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Sifre { get; set; }
        public bool Durum { get; set; }
        public bool Admin { get; set; }
    }
}
