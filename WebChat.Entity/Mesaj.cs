﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebChat.Entity
{
    public class Mesaj
    {
        [Key]
        public int ID { get; set; }
        public string GonderenAdi { get; set; }
        public string Icerik { get; set; }
        public string Tarih { get; set; }
    }
}
