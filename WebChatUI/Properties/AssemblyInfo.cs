﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Bir derlemeyle ilgili Genel Bilgiler aşağıdaki öznitelik kümesi kullanılarak 
// denetlenir. Bir derlemeyle ilişkilendirilmiş bilgileri değiştirmek için bu
// öznitelik değerlerini değiştirin.
[assembly: AssemblyTitle("WebChatUI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("WebChatUI")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible özniteliğini false olarak ayarlarsanız COM bileşenleri bu 
// derlemedeki türleri göremez. Bu derlemedeki bir türe COM'dan erişmeniz 
// gerekiyorsa, o türde ComVisible özniteliğini true olarak ayarlayın.
[assembly: ComVisible(false)]

// Bu proje COM'a açılmışsa aşağıdaki GUID typelib'in ID'si içindir
[assembly: Guid("b847387e-a5d2-40da-881a-8941e1940150")]

// Bir derlemenin sürüm bilgisi aşağıdaki dört değerden oluşur:
//
//      Ana Sürüm
//      Alt Sürüm 
//      Yapı Numarası
//      Düzeltme
//
// Tüm değerleri belirtebilirsiniz veya Düzeltme ve Yapı Numaralarına aşağıda 
// gösterildiği gibi '*' ile varsayılan değer atayabilirsiniz:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
