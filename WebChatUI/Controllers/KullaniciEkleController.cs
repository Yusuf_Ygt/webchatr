﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebChat.Business;
using WebChat.Entity;

namespace WebChatUI.Controllers
{
    [AllowAnonymous]
    public class KullaniciEkleController : Controller
    {
        // GET: KullaniciEkle

        KullaniciBusiness Kb = new KullaniciBusiness();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Kullanici kullanici)
        {
            var kullaniciadivarmi = Kb.KullaniciAdiVarmi(kullanici);
            if (kullaniciadivarmi != null)
            {
                ViewBag.mesaj = "Böyle bir kullanıcı var";
                return View();
            }
            else
            {
                kullanici.Admin = false;
                kullanici.Durum = true;
                Kb.Ekle(kullanici);
                ViewBag.mesaj = "Kayit başarılı";
                return View();
            }
        }
    }
}