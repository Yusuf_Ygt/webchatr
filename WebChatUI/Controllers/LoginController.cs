﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebChat.Business;
using WebChat.Entity;

namespace WebChatUI.Controllers
{
    
    public class LoginController : Controller
    {
        // GET: Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(Kullanici kullanici)
        {
            KullaniciBusiness kullanicivarmi = new KullaniciBusiness();
            var kullanicidb = kullanicivarmi.KullaniciVarmi(kullanici);
            if (kullanicidb != null)
            {
                if (kullanicidb.Durum == false)
                {
                    ViewBag.mesaj = "Geçersiz Kullanıcı Adı veya Şifre";
                    return View();
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(kullanicidb.Ad, false);
                    return RedirectToAction("Index","Mesaj");
                }
            }
            else
            {
                ViewBag.mesaj = "Geçersiz Kullanıcı Adı veya Şifre";
                return View();
            }
        }
        public ActionResult KullaniciEkle()
        {
            return RedirectToAction("Index", "KullaniciEkle");
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}