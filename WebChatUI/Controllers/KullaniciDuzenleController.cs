﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebChat.Business;
using WebChat.Entity;

namespace WebChatUI.Controllers
{
    public class KullaniciDuzenleController : Controller
    {
        // GET: KullaniciDuzenle
        KullaniciBusiness kb = new KullaniciBusiness();
        MesajBusiness mb = new MesajBusiness();
        LoginName ln = new LoginName();
        public ActionResult Index()
        {
            Kullanici kullanici = new Kullanici();
            kullanici.Ad = ln.Getir();
            var gelenkullanici = kb.KullaniciAdiVarmi(kullanici);
            if (gelenkullanici == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                return View(gelenkullanici);
            }
        }

        [HttpPost]
        public ActionResult Index(Kullanici kullanici)
        {
            Kullanici dbkullanici = new Kullanici();
            dbkullanici.Ad = ln.Getir();
            dbkullanici = kb.KullaniciAdiVarmi(dbkullanici);
            var kullaniciadivarmi = kb.KullaniciAdiVarmi(kullanici);
            if (kullanici.Ad == null)
            {
                ViewBag.mesaj = "Kullanıcı adı boş geçilemez";
                return View(dbkullanici);
            }
            else if (kullanici.Sifre==null)
            {
                ViewBag.mesaj = "Şifre boş geçilemez";
                return View(dbkullanici);
            }
            else
            {
                
                if (kullaniciadivarmi != null)
                {
                    ViewBag.mesaj = "Böyle bir kullanıcı var";
                    return View(kullaniciadivarmi);
                }
                else
                {
                    dbkullanici.Ad = kullanici.Ad;
                    dbkullanici.Sifre = kullanici.Sifre;
                    kb.Guncelle(dbkullanici);
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Login","Login");
                }
            }
        }
    }
}