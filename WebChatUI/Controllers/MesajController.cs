﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebChat.Business;
using WebChat.Entity;
using WebChatUI.Models;

namespace WebChatUI.Controllers
{
    [Authorize]
    public class MesajController : Controller
    {
        // GET: Mesaj
        MesajBusiness mb = new MesajBusiness();
        KullaniciBusiness kb = new KullaniciBusiness();
        LoginName ln = new LoginName();
        public ActionResult Index()
        {
            
            Kullanici kullanici = new Kullanici();
            kullanici.Ad = ln.Getir();
            kullanici = kb.KullaniciAdiVarmi(kullanici);
            var mesajlar = mb.HepsiniGetir();
            ViewModel model = new ViewModel();
            model.kullanici = kullanici;
            model.mesajlar = mesajlar;
            return View(model);
        }
    }
}