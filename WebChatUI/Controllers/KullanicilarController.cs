﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebChat.Business;
using WebChat.Entity;

namespace WebChatUI.Controllers
{
    [Authorize]
    public class KullanicilarController : Controller
    {
        // GET: Kullanicilar

        KullaniciBusiness kb = new KullaniciBusiness();
        MesajBusiness mb = new MesajBusiness();
        LoginName ln = new LoginName();
        public ActionResult Index()
        {
            Kullanici kullanici = new Kullanici();
            kullanici.Ad = ln.Getir();
            var gelenkullanici = kb.KullaniciAdiVarmi(kullanici);
            if (gelenkullanici.Admin == true)
            {
                var kullanicilar = kb.HepsiniGetir();
                return View(kullanicilar);
            }
            else
            {
                return RedirectToAction("Index", "Mesaj");
            }

        }
        public ActionResult Sil(int id)
        {
            var silinecekkullanici = kb.IdyeGoreGetir(id);
            if (silinecekkullanici == null)
            {
                return HttpNotFound();
            }
            else
            {
                var mesajlar = mb.HepsiniGetir();
                var silinecekmesajlar = from x in mesajlar
                                        where x.GonderenAdi == silinecekkullanici.Ad
                                        select x;
                if (silinecekmesajlar != null)
                {
                    foreach (var item in silinecekmesajlar)
                    {
                        mb.Sil(item);
                    }
                }
                kb.Sil(silinecekkullanici);
                return RedirectToAction("Index");
            }
        }
        public ActionResult MesajlariSil(string id)
        {
            var mesajlar = mb.HepsiniGetir();
            var silinecekmesajlar = from x in mesajlar
                                    where x.GonderenAdi == id
                                    select x;
            if (silinecekmesajlar != null)
            {
                foreach (var item in silinecekmesajlar)
                {
                    mb.Sil(item);
                }
            }
            return RedirectToAction("Index");
        }
    }
}