﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(WebChatUI.Startup))]

namespace WebChatUI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}