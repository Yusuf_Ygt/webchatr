﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebChat.Entity;

namespace WebChatUI.Models
{
    public class ViewModel
    {
        public List<Mesaj> mesajlar { get; set; }
        public Kullanici kullanici { get; set; }
    }
}