﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using WebChat.Business;
using WebChat.Entity;

namespace WebChatUI
{
    public class ChatHub : Hub
    {
        public void Send(string message)
        {
            MesajBusiness mesajekle = new MesajBusiness();
            Mesaj yenimesaj = new Mesaj();
            yenimesaj.Icerik = message;
            yenimesaj.Tarih = DateTime.Now.ToString();
            yenimesaj.GonderenAdi = HttpContext.Current.User.Identity.Name;
            mesajekle.Ekle(yenimesaj);
            Clients.All.sendMessage(HttpContext.Current.User.Identity.Name, message);

        }
    }
}