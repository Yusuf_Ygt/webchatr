﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebChat.Entity;

namespace WebChat.Data.Context
{
    public class WebChatContext : DbContext
    {
        public WebChatContext() : base("WebChatContext")
        {

        }
        public virtual DbSet<Kullanici> Kullanicilar{get; set;}
        public virtual DbSet<Mesaj> Mesajlar { get; set; }
    }
}
