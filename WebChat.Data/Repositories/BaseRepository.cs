﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebChat.Data.Context;

namespace WebChat.Data.Repositories
{
    public class BaseRepository<TEntity> where TEntity:class
    {
        WebChatContext context = new WebChatContext();
        public void Ekle(TEntity nesne)
        {
            context.Set<TEntity>().Add(nesne);
            context.SaveChanges();
        }
        public void Guncelle(TEntity nesne)
        {
            context.Entry(nesne).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Sil(TEntity nesne)
        {
            context.Set<TEntity>().Remove(nesne);
            context.SaveChanges();
        }
        public TEntity IDyeGoreGetir(int id)
        {
            return context.Set<TEntity>().Find(id);
        }
        public List<TEntity> HepsiniGetir()
        {
            return context.Set<TEntity>().ToList();
        }
    }
}
