﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebChat.Data.Context;
using WebChat.Entity;

namespace WebChat.Data.Repositories
{
    public class KullaniciRepository
    {
        WebChatContext context = new WebChatContext();
        public void Ekle(Kullanici nesne)
        {
            context.Kullanicilar.Add(nesne);
            context.SaveChanges();
        }
        public void Guncelle(Kullanici nesne)
        {
            context.Entry(nesne).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Sil(Kullanici nesne)
        {
            context.Kullanicilar.Remove(nesne);
            context.SaveChanges();
        }
        public Kullanici IDyeGoreGetir(int id)
        {
            return context.Kullanicilar.Find(id);
        }
        public List<Kullanici> HepsiniGetir()
        {
            return context.Kullanicilar.ToList();
        }
        public Kullanici KullaniciVarmi(Kullanici kullanici )
        {
            return context.Kullanicilar.FirstOrDefault(x => x.Ad == kullanici.Ad && x.Sifre == kullanici.Sifre);
        }
        public Kullanici KullaniciAdiVarmi(Kullanici kullanici)
        {
            return context.Kullanicilar.FirstOrDefault(x => x.Ad == kullanici.Ad);
        }
    }
}
