﻿namespace WebChat.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kurulum : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kullanicis",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ad = c.String(),
                        Sifre = c.String(),
                        Durum = c.Boolean(nullable: false),
                        Admin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Mesajs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GonderenID = c.Int(nullable: false),
                        AlanID = c.Int(nullable: false),
                        Icerik = c.String(),
                        Tarih = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Mesajs");
            DropTable("dbo.Kullanicis");
        }
    }
}
