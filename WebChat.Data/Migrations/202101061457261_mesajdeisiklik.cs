﻿namespace WebChat.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mesajdeisiklik : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mesajs", "GonderenAdi", c => c.Int(nullable: false));
            DropColumn("dbo.Mesajs", "GonderenID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Mesajs", "GonderenID", c => c.Int(nullable: false));
            DropColumn("dbo.Mesajs", "GonderenAdi");
        }
    }
}
