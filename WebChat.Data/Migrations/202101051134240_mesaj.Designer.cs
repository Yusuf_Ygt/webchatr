﻿// <auto-generated />
namespace WebChat.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class mesaj : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(mesaj));
        
        string IMigrationMetadata.Id
        {
            get { return "202101051134240_mesaj"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
