﻿namespace WebChat.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mesajde : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Mesajs", "GonderenAdi", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Mesajs", "GonderenAdi", c => c.Int(nullable: false));
        }
    }
}
