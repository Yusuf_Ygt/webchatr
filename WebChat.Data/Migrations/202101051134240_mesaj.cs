﻿namespace WebChat.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mesaj : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Mesajs", "AlanID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Mesajs", "AlanID", c => c.Int(nullable: false));
        }
    }
}
