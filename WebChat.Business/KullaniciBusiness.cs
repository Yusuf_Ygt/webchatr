﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebChat.Data.Repositories;
using WebChat.Entity;

namespace WebChat.Business
{
    public class KullaniciBusiness
    {
        KullaniciRepository repo = new KullaniciRepository();
        public void Ekle(Kullanici nesne)
        {
            repo.Ekle(nesne);
        }
        public void Guncelle(Kullanici nesne)
        {
            repo.Guncelle(nesne);
        }
        public void Sil(Kullanici nesne)
        {
            repo.Sil(nesne);
        }
        public Kullanici IdyeGoreGetir(int id)
        {
           return repo.IDyeGoreGetir(id);
        }
        public List<Kullanici> HepsiniGetir()
        {
            return repo.HepsiniGetir();
        }
        public Kullanici KullaniciVarmi(Kullanici kullanici)
        {
            return repo.KullaniciVarmi(kullanici);
        }
        public Kullanici KullaniciAdiVarmi(Kullanici kullanici)
        {
            return repo.KullaniciAdiVarmi(kullanici);
        }
    }
}
